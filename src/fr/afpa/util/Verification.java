package fr.afpa.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Random;
import java.util.Scanner;
public class Verification {
	
/**
 * Methode pour verifier qu'un mot ne comporte pas de chiffres,
 * @param motAverifier : le mot à verifier par la methode,
 * @return vrai si ne contient pas de chiffres, faux si la methode rencontre un chiffre dans la methode.
 */
	public static boolean siPasDeChiffresDansUnMot(String motAverifier) {
		boolean a=true;
		for (int i = 0; i < motAverifier.length(); i++) {
			if(Character.isDigit(motAverifier.charAt(i))){
			a=false; break; //si le charactere à l'index est un chiffre le boolean passe a faux et sort de la boucle.	
			}
		}
		return a;
	}
	
	
	
	
	
	/**
	 * Methode pour savoir si la date entr�e est valide
	 * @param dateAVerifier date � verifier est prise en parametres
	 * @return vrai si la date est bien ecrite, faux dans le cas contraire
	 */
	public static boolean verifierSiUneSeuleDateValide(String dateAVerifier) {

		try {
			String []stockageDate  =dateAVerifier.split("/");
			LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
			return true;//si pas d'erreur renvoit true
		}
		catch(DateTimeParseException| ArrayIndexOutOfBoundsException gr) {
			System.out.println("Date Invalide");//si erreur renvoit false et affiche l'erreur
			return false;
		}
	}
	
	/**
	 * Methode pour renvoyer le type de l'operation effectu�
	 * @param nombreDejours le nombre de jours est negatif ou positif
	 * @return si iun nombre est negatif alors il renvoit le R pour remboursement et p pour payement
	 */
	public static char natureDeOperation(int nombreDejours) {
	char a=' ';
		if(nombreDejours<0) {
			a='r';
		}
		else {
			a='p';
		}
		return a;
	}
	
	
	/**
	 * Methode pour savoir si les dates entrées sont valides
	 * @param dateDeDebutAVerifier la date qu'on souhaite verifier
	 * @return vrai si les deux dates sont valides et faux si les dates entrees ne sont pas validés.
	 */
	public static boolean siDeuxDatesValidesEtDansBonSens(String dateDeDebutAVerifier, String dateDeFinAVerifier) {
		boolean siValide=true;
		LocalDate dateDebut=null;
		LocalDate dateFin=null;
		//verifie que les deux dates sont correctes
		try {
			String[] tableauDateDebut = dateDeDebutAVerifier.split("/");
			dateDebut = LocalDate.parse(tableauDateDebut[2]+"-"+tableauDateDebut[1]+"-"+tableauDateDebut[0]);
			String[] tableauDateFin = dateDeDebutAVerifier.split("/");
			dateFin = LocalDate.parse(tableauDateFin[2]+"-"+tableauDateFin[1]+"-"+tableauDateFin[0]);
			siValide=true;
		}
		catch(DateTimeParseException| ArrayIndexOutOfBoundsException gr){

			siValide=false;

		}
		//verification que les deux dates sont la premiere apres la deuxieme et pas l'inverse
		if(!siValide) {
			System.out.println("Les dates entrées sont incorrectes!");
			return false;
		}
		else {
			if(dateDebut.isAfter(dateFin))
			{
				System.out.println("La date de debut ne peut se trouver apres la date de fin");
				return false;
			}
			else return true;
		}
	}

	/**
	 * Methode pour transformer une chaine de caracteres en date
	 * @param dateaATransformer la date qu'on souhaite transformer en date
	 * @return la date type LocalDate
	 */
	public static LocalDate transformerDate(String dateaATransformer) {
		LocalDate versionDate=null;

		String [] dateVer1=dateaATransformer.split("/");
		versionDate=LocalDate.parse(dateVer1[2]+"-"+dateVer1[1]+"-"+dateVer1[0]);
		return versionDate;
	}

	/**
	 * Methode pour verifier si le mot en parametres est compos� de chiffres et si
	 * sa longeur est egale � 10
	 * @param motAVerifier Principalement l'identifiant du client et son numero de telephone
	 * @return vrai si le mot est bien compos� de 10 chiffres, sinon retourne faux.
	 */
	public static boolean siEstUnChiffreEtLongueurDix(String motAVerifier) {

		if(motAVerifier.length() == 10) {//si la longeur est de 10
			try {
				Integer.parseInt(motAVerifier);//verification qu'il n'y a que des chiffres
				return true; 
			}
			catch (NumberFormatException siErreur) {//il y a erreur si il y a des lettres
				System.out.println("Identifiant invalide, l'identifiant doit compose uniquement chiffres");
				return false;
			}
		}
		return  false;//si la longeur n'est pas 10
	}
	/**
	 * Methode pour verifier le login de l'employe
	 * @param loginEmp on prend le login que l'utilisarteur va rentrer
	 * @return vrai si tout est correct, faux s'il y a une erreur
	 */
	public static boolean verifLoginEmploye(String loginEmp) {
		//si la longeur est de 5
		if(loginEmp.length()==5) {   
			//si les premieres lettres sont GH
			if(loginEmp.substring(0,2).equals("GH")) {
				//recuperation du reste de la chaine de caracteres pour la parcourir
				String a= loginEmp.substring(3,loginEmp.length());
				//Verification que ce ne sont que des chiffres
				try{ 
					Integer.parseInt(a);
					return true;
				}
				catch(NumberFormatException chope) //si il rencontre des lettre=erreur
				{
					return false;
				}
					
				}
			return false;
			}
			else 
			return false;
	}	
	/**
	 * Methode pour generer un code � 10chiffres al�atoires		
	 * @return le code de 10 chiffres
	 */
	public static String creationIdentifiantClient() {
		   Random nouveauchiffre = new Random(); 
		   String identifiant="";
		   for (int i = 0; i < 10; i++) {
			
		   identifiant=identifiant+nouveauchiffre.nextInt(10);
		   }
		   return identifiant;
	  }


	/**Methode pour verifier si la personne est majeure ou non
	 * 
	 * @param age un entier qu'on compare,
	 * @return vrai si la personne a 18 ans ou plus et faux dans le cas contraire
	 */
	public static boolean verificationAgeClient(int age) {

		if(age >= 18) {
			return true;
		}
		else 
			return false;

	}


	/**
	 * Verification du mot de passe admin
	 * 
	 * @return	true si le mot de passe est correct
	 * 			false si non
	 * @throws IOException
	 */
	public static boolean logAdminCorrect(Scanner in) throws IOException {
		// ouverture du fichier de mot de passe admin
		BufferedReader br = new BufferedReader(new FileReader("ressources\\LogAdmin.txt"));
		String mdpAdmin = br.readLine(); // lecture du mdp
		br.close();
		System.out.println("Mot de Passe :");
		if (!in.nextLine().equals(mdpAdmin)) // entree du mot de passe et verification, si different alors erreur
		{
			System.out.println("erreur de mot de passe");
			return false;
		}
		return true;
	}


public static boolean verifAdresseMailClient(String emailClient) {

	boolean validePartie1 =false;
	boolean validePartie2 =false;
	boolean validePartie3 =false;
	boolean validePartie4 =false;

	//System.out.println("Entrer votre adresse mail");
	
	String [] email = emailClient.split("@");
	if(email.length!=2) {
		System.out.println("Erreur de saisie");
		return false;
		}
	String [] email2 = email[1].split("\\.");
	if(email2.length!=2) {
		System.out.println("Erreur de saisie");	
		return false; 

}
	
	if(email[0].charAt(0)==45 || email[0].charAt(0)==46 || email[0].charAt(0)==95 || email[0].charAt(email[0].length()-1)==46) {
		System.out.println("Erreur de saisie");
		validePartie1=  false;
		}
	else 
		validePartie1=  true;
	
		for (int i = 0; i < email[0].length(); i++) {
			
			if((email[0].charAt(i) > 47 && email[0].charAt(i) < 58) 
					|| (email[0].charAt(i) > 64 && email[0].charAt(i) < 91) 
					|| (email[0].charAt(i) > 96 && email[0].charAt(i) < 123)
					|| email[0].charAt(i) == 45
					|| email[0].charAt(i) == 46
					|| email[0].charAt(i) == 95)
				
				validePartie2 =  true;
			else {
				System.out.println("Erreur de saisie");
				validePartie2 =  false;
				break;}
		}
		
		for (int j = 0; j < email2[0].length(); j++) {
			
			if((email[1].charAt(j) > 64 && email[1].charAt(j) < 91) 
					|| (email[1].charAt(j) > 96 && email[1].charAt(j) < 123)
					)
				validePartie3 =  true;
			else {
				System.out.println("Erreur de saisie");
				validePartie3 =  false;
				break;}
		}
	
		
		
for (int k = 0; k < email2[0].length(); k++) {
			
			if((email[1].charAt(k) > 64 && email[1].charAt(k) < 91) 
					|| (email[1].charAt(k) > 96 && email[1].charAt(k) < 123)
					)
				validePartie4 =  true;
			else {
				System.out.println("Erreur de saisie");
				validePartie4 =  false;
				break;
				}
		}
		
	

return (validePartie1 && validePartie2 && validePartie3 && validePartie4);	
}
}

