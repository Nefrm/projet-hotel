package fr.afpa.util;


import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import fr.afpa.objects.Client;
import fr.afpa.objects.Employes;

public class Mail {
	
	public static void creationMail(Employes employe, Client client) {
		
	String adresseMailEmploye = employe.getAdresseMail();
	String motDePasseEmploye = employe.getMdpMail();
	String serveur = "smtp.gmail.com";
	
//Propri�t�s
	Properties props = System.getProperties();
	props.put("mail.smtp.host",serveur);
	props.put("mail.smtp.starttls.enable","true");
	props.put("mail.smtp.port",587);
	props.put("mail.smtp.auth","true");
	Session session = Session.getInstance(props);
	
	Message message = new MimeMessage(session);
	
	try {        
		if(client!=null) {

        message.setFrom(new InternetAddress(adresseMailEmploye));
        //destinataire
        InternetAddress[] internetAddresses = new InternetAddress[1];
        internetAddresses[0]=new InternetAddress(client.getEmailClient());
        message.setRecipients(Message.RecipientType.TO,internetAddresses);
        
    //objet du message
        message.setSubject("Facture");
        
    //zone de texte du mail
        MimeBodyPart body = new MimeBodyPart();
        body.setText("Bonjour,\n\nVous trouverez en pièces jointes la facture de votre réservation.\n\nCordialement ");
        
    //Piece jointe fichier
    MimeBodyPart pieceJointe = new MimeBodyPart();
    String cheminFacturePDF = "ressources\\factures\\factures" + client.getIdentifiantClient().toLowerCase()+LocalDate.now()+".pdf";
        pieceJointe.attachFile(cheminFacturePDF);
        
    //relie les parties entre elles
    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(body);
    multipart.addBodyPart(pieceJointe);
    message.setContent(multipart);
    
    //envoi du message
    Transport transport = session.getTransport("smtp");
    transport.connect(serveur,adresseMailEmploye,motDePasseEmploye);
    transport.sendMessage(message, internetAddresses);
    transport.close();
        }   
		else 
			System.out.println("erreur400");
        
    } catch (AddressException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (MessagingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    }
	}

