package fr.afpa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

import fr.afpa.objects.Chambre;
import fr.afpa.objects.Client;
import fr.afpa.objects.Employes;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

import fr.afpa.objects.Hotel;

public class PDF {

	public static void creationPdf(Client client, Chambre chambre, Hotel tirets, Employes employe, LocalDate dateDonnee) throws IOException {
		
		String montant="";
		String natureOperation="";
		String details ="";
		FileReader fr = new FileReader("ressources\\transactions\\transactions" + client.getIdentifiantClient().toLowerCase()+dateDonnee+".txt");
		BufferedReader ef = new BufferedReader(fr);
		if (ef.ready())	//on ignore la premiere ligne
		ef.readLine();
		
		if(ef.ready())// on recupere la ligne qui contient l'information sur le type d'operation effectue pour l'affichage
		natureOperation = ef.readLine().toUpperCase();
		//TODO 
		
		switch (natureOperation.charAt(0)) {
		
		case 'P': natureOperation="Payement";break;
		
		case 'R':natureOperation="Remboursement";break;
			
		default: natureOperation="Erreur";break; 
		}
		if(ef.ready())
		details=ef.readLine().toUpperCase();//recuperation de la ligne ou sont stock� les details reservation/modification/autre
		
		switch(details.charAt(0)) {
		case 'R': details="Reservation"; break;
		case 'M': details="Modification de votre reservation"; break;
		case 'A': details="Annulation de la reservation"; break;
		default: details="Erreur"; break;
		}
		
		if(ef.ready())
		montant=ef.readLine();//recuperation de la ligne qui contient le montant pay�
		ef.close();
		
		if(!(new File("ressources\\factures").exists()))
			new File("ressources\\factures").mkdir();
		String chemin="ressources\\factures\\factures" + client.getIdentifiantClient().toLowerCase()+dateDonnee+".pdf";
		Document facture=null;

		
		
		try {
			// Creation Pdf
			PdfWriter writer= new PdfWriter(chemin);
			PdfDocument pdf= new PdfDocument (writer);
			facture = new Document(pdf, PageSize.A4);


			facture.add(new Paragraph("Hôtel Java").setBold().setFontSize(20).setTextAlignment(TextAlignment.CENTER));

			Paragraph infoClient =new Paragraph();
			infoClient.add(client.getNomClient()+" "+client.getPrenomClient()+"\nIdentifiant client: "+client.getIdentifiantClient()+"\nNumero de telephone du Client :"+client.getNumeroTelephoneClient());

			facture.add(infoClient);

			Paragraph titre =new Paragraph().setFixedPosition(33, 613, 200);
			titre.add("Facture \n").setBold().setFontSize(15);
			facture.add(titre);

			Paragraph dateDePayement = new Paragraph().setFixedPosition(405,613,200);
			dateDePayement.add("Date : " + dateDonnee );
			facture.add(dateDePayement);

			Table operationEffectue= new Table(2).setHeight(250).setFixedPosition(33, 340, 540);

			operationEffectue.addHeaderCell(new Cell().add(new Paragraph("Description de l'opération").setWidth(200)).setBackgroundColor(ColorConstants.GRAY));
			operationEffectue.addHeaderCell(new Cell().add(new Paragraph("Prix")).setBackgroundColor(ColorConstants.GRAY));

			
			operationEffectue.addCell(natureOperation+"\nVous avez effectué une "+details);
			operationEffectue.addCell(chambre.getTarif()+"€ par jour");
			
									
			Cell cell = new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER);
			operationEffectue.addCell(cell);
			
			cell = new Cell().add(new Paragraph("Montant total")).setHeight(20).setBackgroundColor(ColorConstants.GRAY);
			operationEffectue.addCell(cell);
			cell = new Cell().add(new Paragraph("")).setBorder(Border.NO_BORDER);
			operationEffectue.addCell(cell);
			cell = new Cell().add(new Paragraph("Total "+montant+"€")).setHeight(20);
			operationEffectue.addCell(cell);
			facture.add(operationEffectue);


			Paragraph infoEmploye =new Paragraph().setFixedPosition(33, 270, 200).setFontSize(10).setFontColor(ColorConstants.GRAY);
			infoEmploye.add("Facture générée par: \n"+employe.getNom()+" "+employe.getLogin());
			facture.add(infoEmploye);

		
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		finally {

			facture.close();
		}









	}


}




