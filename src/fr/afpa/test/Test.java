package fr.afpa.test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.objects.*;
import fr.afpa.util.*;

public class Test {

	static Hotel California = new Hotel(true);

	// Test de Login
	public static void loginClient() {
		
		// TODO
	}

	public static void loginEtMDPEmploye() {
		// TODO
	}

	public static void clientAucuneReservation() {
		// TODO
	}

	public static void clientUneReservation() {
		// TODO
	}

	public static void clientPlusieursReservation() {
		// TODO
	}

	public static void loginAdmin() {

		// TODO
	}

	//TEST METHODES
	//Hotel
	public static void testEstUnClient() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		h.estUnClient(in);
	}
	
	public static void testPremiereChambreLibre() {
		Hotel h = new Hotel(true);
		if(h.premiereChambreLibre(LocalDate.now()) == 1)
			System.out.println("OK");
		else
			System.out.println("KO");
	}
	
	public static void testDerniereChambreLibre() {
		Hotel h = new Hotel(true);
		if(h.derniereChambreLibre(LocalDate.now()) == 1)
			System.out.println("OK");
		else
			System.out.println("KO");
	}
	
	public static void testReservationPremiereChambre() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		int pcl = h.premiereChambreLibre(LocalDate.now());
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusDays(7), null, in);
		} catch (IOException e) {
			System.out.println("KO");
		}
		if(pcl+1 == h.premiereChambreLibre(LocalDate.now()))
			System.out.println("OK");
		else
			System.out.println("KO");
	}
	
	//Chambre
	public static void testEstReservee() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			//reservation de la premiere chambre
			System.out.println("1e chambre");
			if(h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusDays(7), null, in))
				System.out.println("OK");
			else
				System.out.println("KO");
			System.out.println("1e chambre meme moment");
			//reservation a un autre moment
			if(h.reservationPremiereChambreLibre(LocalDate.now().plusDays(8), LocalDate.now().plusDays(10), null, in))
				System.out.println("OK");
			else
				System.out.println("KO");
			//echec de la reservation de la meme chambre au meme moment
			if(!h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusDays(7), null, in))
				System.out.println("OK");
			else
				System.out.println("KO");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void testChambreFirstReservationEmpty() {
		Hotel h = new Hotel(true);
		if(h.getChambreHotel()[0].firstReservationEmpty() == 0)
			System.out.println("OK");
		else
			System.out.println("KO");
	}
	
	//Reservation
	public static void testGenerationTransactionBancaire() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			//reservation de la premiere chambre
			if(h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in)) {
				System.out.println("OK");
			}
			else
				System.out.println("KO");
		} catch (IOException e) {
			System.out.println("KO");
		}
	}
	
	//Modfication
	public static void testModificationReservation() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			System.out.println("--création d'une chambre--");
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			System.out.println("--modification--");
			h.modificationReservation("1", LocalDate.now(), LocalDate.now().plusWeeks(1), LocalDate.now().plusDays(3), LocalDate.now().plusWeeks(1).plusDays(3), in);
			if(h.getChambreHotel()[0] != null && h.getChambreHotel()[0].getOneReservation(0) != null) {
				System.out.println(h.getChambreHotel()[0].getOneReservation(0).toString());
				System.out.println("OK");
			}
			else
				System.out.println("KO");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void testLiberationReservation() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			System.out.println("via le nom");
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			h.afficherNombreChambresReservees(LocalDate.now());
			h.liberationChambre("1", -1, LocalDate.now(), LocalDate.now().plusWeeks(1));
			h.afficherNombreChambresReservees(LocalDate.now());
			System.out.println("via le numero");
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			h.afficherNombreChambresReservees(LocalDate.now());
			h.liberationChambre(null, 1, LocalDate.now(), LocalDate.now().plusWeeks(1));
			h.afficherNombreChambresReservees(LocalDate.now());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void testAnnulationReservation() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			h.afficherNombreChambresReservees(LocalDate.now());
			h.annulationReservation(1, LocalDate.now(), LocalDate.now().plusWeeks(1), in);
			h.afficherNombreChambresReservees(LocalDate.now());
			//TODO
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//PDF
	public static void testCreationPDF() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			if(h.getChambreHotel()[0].getOneReservation(0) != null)
				PDF.creationPdf(h.getChambreHotel()[0].getOneReservation(0).getClientReservation(), h.getChambreHotel()[0], h, h.getEmployeHotel()[0], LocalDate.now());
			else
				System.out.println("KO");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Mail
	public static void testEnvoiDeMail() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusWeeks(1), null, in);
			Mail.creationMail(h.getEmployeHotel()[0], h.getChambreHotel()[0].getReservation()[0].getClientReservation());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void testCalculChiffreAffaire() {
		Hotel h = new Hotel(true);
		Scanner in = new Scanner(System.in);
		try {
			h.reservationPremiereChambreLibre(LocalDate.now(), LocalDate.now().plusDays(7), null, in);
			System.out.println("chiffre :"+h.calculChiffreAffaire(LocalDate.now()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//test de toute les methodes
		System.out.println(California.pleinDeTirets(19) + "test Methodes" + California.pleinDeTirets(20));
		System.out.println(California.pleinDeTirets(23) + "Hotel" + California.pleinDeTirets(24));
		System.out.println(California.pleinDeTirets(20) + "estUnClient" + California.pleinDeTirets(21));
		testEstUnClient();
		System.out.println(California.pleinDeTirets(13) + "LiberationReservation" + California.pleinDeTirets(13));
		testLiberationReservation();
		System.out.println(California.pleinDeTirets(13) + "AnnulationReservation" + California.pleinDeTirets(13));
		testAnnulationReservation();
		System.out.println(California.pleinDeTirets(13) + "ModificationReservation" + California.pleinDeTirets(13));
		testModificationReservation();
		System.out.println(California.pleinDeTirets(24) + "PDF" + California.pleinDeTirets(25));
		System.out.println(California.pleinDeTirets(19) + "CreationDuPDF" + California.pleinDeTirets(20));
		testCreationPDF();
		System.out.println(California.pleinDeTirets(14) + "Calcul Chiffre d'affaire" + California.pleinDeTirets(14));
		testCalculChiffreAffaire();
		System.out.println(California.pleinDeTirets(22) + "Chambre" + California.pleinDeTirets(23));
		System.out.println(California.pleinDeTirets(20) + "estReservee" + California.pleinDeTirets(21));
		testEstReservee();
		System.out.println(California.pleinDeTirets(16) + "firstReservationEmpty" + California.pleinDeTirets(15));
		testChambreFirstReservationEmpty();
		System.out.println(California.pleinDeTirets(22) + "Employee" + California.pleinDeTirets(22));
		System.out.println(California.pleinDeTirets(16) + "premiereChambreLibre" + California.pleinDeTirets(16));
		testPremiereChambreLibre();
		System.out.println(California.pleinDeTirets(13) + "reservationPremiereChambre" + California.pleinDeTirets(13));
		testReservationPremiereChambre();
		System.out.println(California.pleinDeTirets(11) + "generationTransactionBancaire" + California.pleinDeTirets(12));
		testGenerationTransactionBancaire();
	}

}
