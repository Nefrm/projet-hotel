package fr.afpa.objects;

public class Employes {
	// Attributs
	private String nom;
	private String login;
	private String mdp;
	private String adresseMail;
	private String mdpMail;

	// Constructeur
	public Employes(String name, String log, String mdp_, String adresseMail_, String mdpMail_) {

		nom = name;
		login = log;
		mdp = mdp_;
		setAdresseMail(adresseMail_);
		setMdpMail(mdpMail_);

	}

	//Les Set et Get de la classe Employes

	public void setNom(String nom_) {
		nom = nom_;
	}

	public String getNom() {
		return nom;
	}

	public void setLogin(String log_) {
		login = log_;
	}

	public String getLogin() {
		return login;
	}

	public void setMdp(String motdp) {
		mdp = motdp;
	}

	public String getMdp() {
		return mdp;
	}

	public String getAdresseMail() {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail_) {
		adresseMail = adresseMail_;
	}

	public String getMdpMail() {
		return mdpMail;
	}

	public void setMdpMail(String mdpMail_) {
		mdpMail = mdpMail_;
	}

}
