package fr.afpa.objects;

import java.time.LocalDate;

public class Chambre {

	// Attributs
	private Reservation[] reservation;
	private int numeroChambre;
	private String superficieChambre;
	private int tarif;
	private String typeChambre;
	private String vues;
	private String[] listeOption;
	private String occupation;

	// Constructeur
	public Chambre(Reservation[] reservation_, int numChambre, String typChambre, String superfChambre, String vues_, String occup, int tarif_,
			String[] listeOption_) {

		reservation = reservation_;
		numeroChambre = numChambre;
		superficieChambre = superfChambre;
		tarif = tarif_;
		typeChambre = typChambre;
		listeOption = listeOption_;
		vues = vues_;
		occupation = occup;
	}

	/**
	 * Methode pour savoir si la chambre est reservée
	 * 
	 * @param uneDatte la date donnée
	 * @return true si la chambre est reservée et false si la chambre est libre
	 */
	public boolean estReservee(LocalDate uneDatte) {
		for (int i = 0; i < reservation.length; i++) {
			if (reservation[i] != null && reservation[i].getDateDebut() != null && !uneDatte.isBefore(reservation[i].getDateDebut())
					&& !uneDatte.isAfter(reservation[i].getDateFin())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Fonction pour connaitre la premiere reservation libre
	 * 
	 * @return Retourne le numero de la premiere chambre vide, sinon retourne -1
	 */
	public int firstReservationEmpty() {
		for (int i = 0; i < reservation.length; i++) {
			if (reservation[i] == null) {
				return i;
			}

		}
		return -1;
	}

	@Override
	public String toString() {
		String resultat = "Chambre n�"+numeroChambre+"\n -Superficie :"+superficieChambre+"\n -Tarif :"+tarif+"\n -Type :"+typeChambre+"\n -Vue :"+vues+"\n -Option :";
		for (int i = 0; i < listeOption.length; i++) {
			resultat += "\n    -"+listeOption[i];
		}
		return resultat;
	}

	// Les Gets Et les Sets de la classe Chambre
	public int getNumeroChambre() {
		return numeroChambre;
	}

	public void setNumeroChambre(int numChambre) {
		numeroChambre = numChambre;
	}

	public String getSuperficie() {
		return superficieChambre;
	}

	public void setSuperficie(String superfChambre) {
		superficieChambre = superfChambre;
	}

	public int getTarif() {
		return tarif;
	}

	public void setTarif(int tarif_) {
		tarif = tarif_;
	}

	public String getType() {
		return typeChambre;
	}

	public void setType(String typChambre) {
		typeChambre = typChambre;
	}

	public String[] getListeOption() {
		return listeOption;
	}

	public void setListeOption(String[] listOption) {
		listeOption = listOption;
	}

	public void setOccupation(String occupation_) {
		occupation = occupation_;
	}

	public String getOccupation() {
		return occupation;
	}

	public Reservation[] getReservation() {
		return reservation;
	}

	public void setReservation(Reservation[] reservation_) {
		reservation = reservation_;
	}

	public Reservation getOneReservation(int indice) {
		return reservation[indice];
	}

	public void setOneReservation(Reservation reservation_, int indice) {
		reservation[indice] = reservation_;
	}
}
