package fr.afpa.objects;

public class Client {
	
	// Attributs
	private String nomClient;
	private String prenomClient;
	private String identifiantClient;
	private String dateNaissanceClient;
	private int ageClient;
	private String emailClient;
	private String numeroTelephoneClient;

	// Constructeur
	public Client(String nom, String prenom, String identifiant, String dateNaissance, int age, String email,
			String numeroTelephone) {
		nomClient = nom;
		prenomClient = prenom;
		identifiantClient = identifiant;
		dateNaissanceClient = dateNaissance;
		ageClient = age;
		emailClient = email;
		numeroTelephoneClient = numeroTelephone;
	}
	
	// Les GET et les SET de la classe Client
	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nom) {
		nomClient = nom;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenom) {
		prenomClient = prenom;
	}

	public String getIdentifiantClient() {
		return identifiantClient;
	}

	public void setIdentifiantClient(String identifiant) {
		identifiantClient = identifiant;
	}

	public String getDateNaissanceClient() {
		return dateNaissanceClient;
	}

	public void setDateNaissanceClient(String dateNaissance) {
		dateNaissanceClient = dateNaissance;
	}

	public int getAgeClient() {
		return ageClient;
	}

	public void setAgeClient(int age) {
		ageClient = age;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String email) {
		emailClient = email;
	}

	public String getNumeroTelephoneClient() {
		return numeroTelephoneClient;
	}

	public void setNumeroTelephoneClient(String numeroTelephone) {
		numeroTelephoneClient = numeroTelephone;
	}
}
