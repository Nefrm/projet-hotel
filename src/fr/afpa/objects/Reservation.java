package fr.afpa.objects;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.util.Verification;

public class Reservation {
	//Attributs
	private Client client;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	
	//Constructeur
	public Reservation(LocalDate dateDebut_, LocalDate dateFin_, Scanner in) {
		client = creerClient(in);
		dateDebut = dateDebut_;
		dateFin = dateFin_;
	}
	
	public Reservation(LocalDate dateDebut_, LocalDate dateFin_, Client client_) {
		client = client_;
		dateDebut = dateDebut_;
		dateFin = dateFin_;
	}
	//Getter Setter
	public Client getClientReservation() {
		return client;
	}

	public void setClientReservation(Client client_) {
		client = client_;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut_) {
		dateDebut = dateDebut_;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin_) {
		dateFin = dateFin_;
	}
	
	//Méthodes
	/**
	 * generation de la transaction bancaire
	 * 
	 * @param naturePaiment	paiement ou remboursement
	 * @param montant	le prix de la transaction
	 * @param type	le type de transaction
	 * @param in le scanner
	 * @throws IOException
	 */
	public void generationTransactionBancaire(char naturePaiment, double montant , char type, Scanner in) throws IOException {
		String date = LocalDate.now().toString();
		if(!(new File("ressources").exists()))
			new File("ressources").mkdir();
		if(!(new File("ressources\\transactions").exists()))
			new File("ressources\\transactions").mkdir();
		BufferedWriter nouvelleTransaction = new BufferedWriter(new FileWriter("ressources\\transactions\\transactions"+client.getIdentifiantClient().toLowerCase()+date+".txt"));
		nouvelleTransaction.write(date);
		nouvelleTransaction.newLine();
		nouvelleTransaction.write(naturePaiment);
		nouvelleTransaction.newLine();
		nouvelleTransaction.write(type);
		nouvelleTransaction.newLine();
		nouvelleTransaction.write(String.valueOf(montant));
		nouvelleTransaction.newLine();
		String carte = "";
		while (Verification.siEstUnChiffreEtLongueurDix(carte)) {
			System.out.println("numero de carte bancaire : (10 chiffres)");
			carte = in.nextLine();
		}
		nouvelleTransaction.write(carte);
		nouvelleTransaction.close();
	}
	
	/**
	 * Creation du Client
	 * 
	 * @param in le scanner
	 * @return un nouveau Client
	 */
private Client creerClient(Scanner in) {
		
		String nomCl ="";
		String prenomCl ="";
        String dateNaiss ="";
        int age=0;
        String mail = "";
        String tel = "";
        String login = "";

		Boolean nomValide = false;
		
		while (!nomValide) {
			System.out.println("Entrez le nom du client :");
			nomCl = in.nextLine();
			if (Verification.siPasDeChiffresDansUnMot(nomCl))
				nomValide = true;
			else
				nomValide = false;
		}
		
		
		Boolean prenomValide = false;
		while (!prenomValide) {
			System.out.println("Entrez le prénom du client :");
			prenomCl = in.nextLine();
			if (Verification.siPasDeChiffresDansUnMot(prenomCl))
				prenomValide = true;
			else
				prenomValide = false;
		}
		
		Boolean dateNaissanceValide = false;
		while (!dateNaissanceValide) {
			System.out.println("Entrez la date de naissance du client :");
			dateNaiss = in.nextLine();
			if (Verification.verifierSiUneSeuleDateValide(dateNaiss))
				dateNaissanceValide = true;
			else
				dateNaissanceValide = false;
		}
		
		Boolean ageValide = false;
		while (!ageValide) {
			System.out.println("Entrez l'age du client");
			age = in.nextInt();
			in.nextLine();
			if (Verification.verificationAgeClient(age))
				ageValide = true;
			else
				ageValide = false;
		}
		
		Boolean adresseMailValide = false;
		while (!adresseMailValide) {
	    System.out.println("Entrez l'email du client");
		mail = in.nextLine();
		if (Verification.verifAdresseMailClient(mail))
				adresseMailValide = true;
			else
				adresseMailValide = false;
		}
		
		Boolean telephoneValide = false;
		while (!telephoneValide) {
	    System.out.println("Entrez le telephone du client");
		tel = in.nextLine();
		if (Verification.siEstUnChiffreEtLongueurDix(tel))
				telephoneValide = true;
			else
				telephoneValide = false;
		}
	

	   
		login = Verification.creationIdentifiantClient();
		 System.out.println("Votre Login est : "+login);
		

		
		
		Client client = new Client(nomCl, prenomCl, login, dateNaiss, age, mail, tel);
		return client;
	}

}
