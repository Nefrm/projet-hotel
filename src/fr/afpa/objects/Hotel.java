package fr.afpa.objects;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

import fr.afpa.util.Mail;
import fr.afpa.util.PDF;
import fr.afpa.util.Verification;

public class Hotel {
	// Attributs
	private Employes[] employeHotel;
	private Chambre[] chambreHotel;

	// Constructeur
	/**
	 * Constructeur pour utilisateur
	 */
	public Hotel() {
		Scanner in = new Scanner(System.in);
		try {
			initHotel();
			launch(in);
		} catch (IOException e) {
			System.out.println("perdu !");
		}finally {
			in.close();
		}
	}

	/**
	 * Constructeur pour test
	 * 
	 * @param test
	 *            permet de lancer les tests seuls
	 */
	public Hotel(boolean test) {
		try {
			initHotel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Affichage du Menu
	 */
	private void affichageMenu() {

		System.out.println();
		System.out.println(pleinDeTirets(20) + " MENU HOTEL CDA JAVA " + pleinDeTirets(20));
		System.out.println();
		System.out.println();
		System.out.println("A-	Afficher l'état de l'hôtel");
		System.out.println("B-	Afficher le nombre de chambres réservées");
		System.out.println("C-	Afficher le nombre de chambre libres");
		System.out.println("D-	Afficher le num�ro de la première chambre vide");
		System.out.println("E-	Afficher le numero de la dernière chambre vide");
		System.out.println("F-	Réserver une chambre");
		System.out.println("G-	Libérer une chambre");
		System.out.println("H-	Modifier une réservation");
		System.out.println("I-	Annuler une réservation");
		System.out.println("J-	Afficher le chiffre d'affaire");
		System.out.println();
		System.out.println("Q-	Quitter");
		System.out.println();
		System.out.println(pleinDeTirets(74));

	}

	/**
	 * Tirets pour l'affichage menu :)
	 * 
	 * @param nbTirets
	 *            le nombre des tirets pour un meilleur affichage
	 * @return chaine de caracteres avec des tirets
	 */
	public String pleinDeTirets(int nbTirets) {

		String tiret = "";

		for (int i = 0; i < nbTirets; i++) {

			tiret += "-";

		}
		return tiret;
	}

	/**
	 * Methode pour initialiser l'Hotel à partir d'un fichier csv
	 */
	public void initHotel() throws IOException {
		// parcours du fichier pour savoir la taille du tableau
		FileReader fr = new FileReader("ressources\\ListeChambres.csv");
		BufferedReader br = new BufferedReader(fr);
		int compteur = 0;
		if (br.ready())
			br.readLine(); // on ignore la premiere ligne
		while (br.ready()) {
			String[] nombre = br.readLine().split(";");
			compteur += Integer.parseInt(nombre[5]);
		}
		chambreHotel = new Chambre[compteur]; // déclaration de chaque chambre
		br.close();

		// reouverture du fichier pour initialiser chaque chambre
		fr = new FileReader("ressources\\ListeChambres.csv");
		BufferedReader ze = new BufferedReader(fr);
		if (ze.ready()) // on ignore la premiere ligne
			ze.readLine();

		String[] initialisationHotel = new String[compteur];
		compteur = 0;
		int j = 0;
		for (; ze.ready();) {
			initialisationHotel = ze.readLine().split(";");
			compteur += Integer.parseInt(initialisationHotel[5]);
			for (; j < compteur; j++) {
				chambreHotel[j] = new Chambre(new Reservation[5], j + 1, initialisationHotel[0], initialisationHotel[1],
						initialisationHotel[2], initialisationHotel[3], Integer.parseInt(initialisationHotel[4]),
						initialisationHotel[6].split("\\|"));
			}
		}

		ze.close();
		Employes employe = new Employes("Roger", "GH000", "GH000", "employe.hotel@gmail.com", "GH000000" );
		employeHotel = new Employes[1];
		employeHotel[0] = employe;

	}

	/**
	 * Methode pour l'authetification au lancement de l'application
	 * 
	 * @return un boolean, true si c'est un client et false si c'est un employé
	 */
	public void estUnClient(Scanner in) {
		char reponse = 'Y';
		while (reponse != 'E' || reponse != 'C') {
			System.out.println("Connection en tant que (E)mployé ou (C)lient:");
			reponse = in.nextLine().toUpperCase().charAt(0);
			if (reponse == 'C') {
				boolean client = false;
				while (!client) {
					System.out.println("Entrer votre identifiant");
					String idClient = in.nextLine();

					
						for (int i = 0; i < chambreHotel.length && !client; i++) {
							//parcours des reservations 
							for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {
								//pour rechercher le nom du client 
								if (idClient!= null && chambreHotel[i].getOneReservation(j) != null) {
									if (idClient.equalsIgnoreCase(chambreHotel[i].getReservation()[j].getClientReservation().getIdentifiantClient())) {
										client = true;
										affichageClient20Sec(idClient);
										break;
									}
									else {
										System.out.println("Aucun client avec cet identifiant");
									}
								}
								
							}
						}
						if(!client) {
							System.out.println("Pas de client");
						}
					}
							

			} else if (reponse == 'E') {
				boolean employe = false;
				while(!employe) {
					System.out.println("Entrer votre identifiant");
					String idEmploye = in.nextLine();


					if(Verification.verifLoginEmploye(idEmploye)) {
						for (int i = 0; i < employeHotel.length; i++) {
							if(idEmploye.equalsIgnoreCase(employeHotel[i].getLogin())) {
								System.out.println("Entrez votre mot de passe");
								String mdpEmploye=in.nextLine();
								if(mdpEmploye.equals(employeHotel[i].getMdp())){
									choixMenu(in);
									employe=true;
								}
								else {
									System.out.println("Mauvais mot de passe");
								}
							}
							else {
								System.out.println("Mauvais entrée de login");
								
							}
						}
					}

				}


			}
		}
	
	}


	/**
	 * Methode pour afficher les informations pendant 20sec au client
	 * 
	 * @param identifClient
	 */
	public void affichageClient20Sec(String identifClient) {
		try {
			for (int i = 0; i < chambreHotel.length; i++) {
				// parcours des reservations
				for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {
					// pour rechercher l'identifiant du client
					if (chambreHotel[i].getOneReservation(j) != null
							&& chambreHotel[i].getOneReservation(j).getClientReservation() != null) {
						if (identifClient.equalsIgnoreCase(
								chambreHotel[i].getReservation()[j].getClientReservation().getIdentifiantClient())) {
							System.out.println(pleinDeTirets(25) + "Reservation" + pleinDeTirets(25)
									+ "\nDate de debut de votre reservation: "
									+ chambreHotel[i].getReservation()[j].getDateDebut()
									+ "\nDate de fin de votre reservation :"
									+ chambreHotel[i].getReservation()[j].getDateFin());

							java.util.concurrent.TimeUnit.SECONDS.sleep(20);

						} else {
							System.out.println("Vous n'avez aucune reservation.");
						}
					}
				}
			}
		} catch (final java.lang.InterruptedException e) {
			// ToDo - Handle the interruption.
		}

	}
	


	// Les Get et les Set de la classe Hotel

	public Employes[] getEmployeHotel() {
		return employeHotel;
	}

	public void setEmployeHotel(Employes[] employeHotel_) {
		employeHotel = employeHotel_;
	}

	public Chambre[] getChambreHotel() {
		return chambreHotel;
	}

	public void setChambreHotel(Chambre[] chambreHotel_) {
		chambreHotel = chambreHotel_;
	}

	/**
	 * Afficher toutes les chambres occupées et les info de la personne qui
	 * l'occupent + les informations de la reservation
	 * 
	 */
	public void afficherEtatHotel() {

		for (int i = 0; i < chambreHotel.length; i++) {
			System.out.println(chambreHotel[i]);

		}

	}

	/**
	 * Méthode pour Afficher le nombre de chambres reservées
	 * 
	 * @param date
	 *            prend en parametre la date donnée
	 */
	public void afficherNombreChambresReservees(LocalDate date) {
		int compteur = 0;
		for (int i = 0; i < chambreHotel.length; i++) {
			if (chambreHotel[i] != null) {
				if (chambreHotel[i].estReservee(date)) {
					compteur++;
				}
			}
		}
		System.out.println("Le nombre de chambres réservées est de : " + compteur);
	}

	/**
	 * Methode pour affiche le nombre de chambres libres
	 * 
	 * @param date
	 *            prend en parametre la date donn�e
	 */
	public void afficherNombreChambresLibres(LocalDate date) {
		int compteur = 0;
		for (int i = 0; i < chambreHotel.length; i++) {
			if (chambreHotel[i] != null) {
				if (!chambreHotel[i].estReservee(date)) {
					compteur++;
				}
			}
		}
		System.out.println("Le nombre de chambres libres est de : " + compteur);
	}

	/**
	 * Methode pour retourner la premiere chambre libre
	 * 
	 * @param date:
	 *            prend en parametres la date donn�e
	 * @return un entier, la premiere chambre libre.
	 */
	public int premiereChambreLibre(LocalDate date) {
		for (int i = 0; i < chambreHotel.length; i++) {
			if (chambreHotel[i] != null) {
				if (!chambreHotel[i].estReservee(date)) {
					return chambreHotel[i].getNumeroChambre();
				}
			}
		}
		return -1;
	}

	/**
	 * Methode pour retourner la derniere chambre libre
	 * 
	 * @param date:
	 *            prend en parametres la date donnée
	 * @return un entier, la derniere chambre libre.
	 */
	public int derniereChambreLibre(LocalDate date) {
		for (int i = chambreHotel.length - 1; i >= 0; i--) {
			if (chambreHotel[i] != null) {
				if (!chambreHotel[i].estReservee(date)) {
					return chambreHotel[i].getNumeroChambre();
				}
			}
		}
		return -1;
	}

	/**
	 * Reserve la première chambre libre
	 * 
	 * @param dateDebut
	 *            Date de Debut de la reservation
	 * @param dateSortie
	 *            Date de Fin de la reservation
	 * @param in le scanner
	 * @return true si chambre reservée false si chambre pas reservée
	 * @throws IOException
	 */
	public boolean reservationPremiereChambreLibre(LocalDate dateDebut, LocalDate dateFin, Client client, Scanner in)
			throws IOException {
		int disponibilite = 0;
		Chambre[] chambreDisponible = new Chambre[chambreHotel.length];
		if (dateDebut.isAfter(dateFin)) { // verification si la date en parametre est correct
			System.out.println("Date invalide");
			return false;
		}

		// demande et verification pour le nombre de personne
		int adulte = -1;
		int enfant = -1;
		while (adulte < 0 || enfant < 0) { // tant qu'il n'y a pas au moins 1 adulte
			System.out.println("Entrer le nombre d'adulte(s)");
			adulte = in.nextInt();
			in.nextLine();
			System.out.println("Entrer le nombre d'enfant(s)");
			enfant = in.nextInt();
			in.nextLine();
			if (adulte == 0 && enfant > 0) { // si il n'y a que des enfants
				enfant = -1;
				System.out.println("Reservation impossible, au moins un adulte par chambre");
			}
		}

		// recherche de toute les chambres libre
		for (int i = 0; i < chambreHotel.length; i++) {
			boolean estLibre = true;
			// création d'une nouvelle date a la date de début
			LocalDate dateChoisie = LocalDate.of(dateDebut.getYear(), dateDebut.getMonth(), dateDebut.getDayOfMonth());
			// pour chaque chambre, on verifie si elle est disponible
			while (!dateChoisie.isBefore(dateDebut) && !dateChoisie.isAfter(dateFin)) {
				int nbAdulte = Integer.parseInt(chambreHotel[i].getOccupation().split(" ")[0]);
				int nbEnfant = Integer.parseInt(chambreHotel[i].getOccupation().split(" ")[3]);
				if (chambreHotel[i].estReservee(dateChoisie)) { // si la chambre est reservee
					estLibre = false;
					break;
				}
				if (!(adulte <= nbAdulte && enfant <= nbEnfant)) { // recherche de chambre avec le bon nombre d'occupant
					estLibre = false;
					break;
				}
				dateChoisie = dateChoisie.plusDays(1);
			}

			// mise de coté de la chambre libre
			if (estLibre) {
				chambreDisponible[disponibilite] = chambreHotel[i];
				disponibilite++;
				System.out.println(chambreHotel[i]);
			}
		}
		// choix de la chambre et reservation
		if (disponibilite > 0) {
			System.out.println("quelle chambre voulez vous ?");
			int choix = in.nextInt() - 1;
			in.nextLine();
			for (int i = 0; i < chambreDisponible.length; i++) {
				if (chambreDisponible[i] != null && chambreDisponible[i].getNumeroChambre() - 1 == choix
						&& chambreDisponible[i].firstReservationEmpty() != -1) {
					// reservation de la chambre et creation du client
					int indice = chambreHotel[choix].firstReservationEmpty();
					if (client == null)
						chambreHotel[choix].setOneReservation(new Reservation(dateDebut, dateFin, in), indice);
					else
						chambreHotel[choix].setOneReservation(new Reservation(dateDebut, dateFin, client), indice);
					if(chambreHotel[choix] != null && chambreHotel[choix].getOneReservation(indice) != null) {
						long nbJours = ChronoUnit.DAYS.between(chambreHotel[choix].getOneReservation(indice).getDateDebut(),
								chambreHotel[i].getOneReservation(indice).getDateFin());
						double prix = chambreHotel[i].getTarif() * nbJours;
						chambreHotel[choix].getOneReservation(indice).generationTransactionBancaire('P', prix, 'R', in);
						PDF.creationPdf(getChambreHotel()[choix].getOneReservation(indice).getClientReservation(), getChambreHotel()[choix], this, getEmployeHotel()[0], LocalDate.now());
						Mail.creationMail(employeHotel[0], chambreHotel[choix].getOneReservation(indice).getClientReservation());
						return true;
					}
				}
			}
			return false;
		} else {
			System.out.println("aucune chambre de disponible avec ces criteres");
			return false;
		}
	}

	/**
	 * Libere la chambre d'un client via son nom ou son numero de chambre
	 * 
	 * @param nomClient		le nom du client
	 * @param numeroChambre	le numero de chambre
	 * @param dateDebut		la date de début de réservation
	 * @param dateFin		la date de fin de réservation
	 * @return	true si la libération s'est bien passé, false si non
	 * @throws IOException
	 */
	public boolean liberationChambre(String nomClient, int numeroChambre, LocalDate dateDebut, LocalDate dateFin)
			throws IOException {
		for (int i = 0; i < chambreHotel.length; i++) {

			for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {

				if (nomClient != null && chambreHotel[i].getOneReservation(j) != null) {

					if (nomClient
							.equalsIgnoreCase(chambreHotel[i].getOneReservation(j).getClientReservation().getNomClient())
							&& dateDebut.equals(chambreHotel[i].getOneReservation(j).getDateDebut())
							&& dateFin.equals(chambreHotel[i].getOneReservation(j).getDateFin())) {

						chambreHotel[i].getOneReservation(j).setDateDebut(null);
						chambreHotel[i].getOneReservation(j).setDateFin(null);
						chambreHotel[i].getOneReservation(j).setClientReservation(null);
						System.out.println("Libération effectuée avec succès");
						return true;
					}
				} else if (numeroChambre > 0 && chambreHotel[i].getOneReservation(j) != null) {
					if (numeroChambre == (chambreHotel[i].getNumeroChambre())
							&& dateDebut.equals(chambreHotel[i].getOneReservation(j).getDateDebut())
							&& dateFin.equals(chambreHotel[i].getOneReservation(j).getDateFin())) {

						chambreHotel[i].getOneReservation(j).setDateDebut(null);
						chambreHotel[i].getOneReservation(j).setDateFin(null);
						chambreHotel[i].getOneReservation(j).setClientReservation(null);
						System.out.println("Libération effectuée avec succès");
						return true;
					}
				}
			}
		}
		System.out.println("La Reservation est introuvable");
		return false;
	}

	/**
	 * Methode pour modifier la reservation d'une chambre
	 * 
	 * @param nomClient
	 *            : Nom du client qui souhaite faire la modification
	 * @param dateDebut
	 *            la date du debut de la premiere reservation qui sera modifiée
	 * @param dateFin
	 *            la date de fin de la premiere reservation qui sera modifiée
	 * @param nouvelleDateDebut
	 *            la nouvelle date de debut que le client veut reserver
	 * @param nouvelleDateFin
	 *            la nouvelle date de fin que le client veut reserver
	 * @param in le scanner
	 * @return vrai si la modification est reussi, faux dans le cas contraire
	 * @throws IOException
	 */
	public boolean modificationReservation(String identifClient, LocalDate dateDebut, LocalDate dateFin,
			LocalDate nouvelleDateDebut, LocalDate nouvelleDateFin, Scanner in) throws IOException {
		
		int nbDeJoursReservation1=(int)ChronoUnit.DAYS.between(dateDebut, dateFin);
		int nbDeJoursReservation2=(int)ChronoUnit.DAYS.between(nouvelleDateDebut, nouvelleDateFin);
		int differenceDebut1etFin2=(int)ChronoUnit.DAYS.between(dateDebut, nouvelleDateFin);;
		int nbdejoursrestants=0;

		//parcours des chambres 
		for (int i = 0; i < chambreHotel.length; i++) {
			//parcours des reservations 
			for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {
				//pour rechercher le nom du client 
				if (identifClient != null && chambreHotel[i].getOneReservation(j) != null) {
			
					if (identifClient.equalsIgnoreCase(chambreHotel[i].getReservation()[j].getClientReservation().getIdentifiantClient()))

					{
						//sauvegardes des dates dans des variables temporelles
						LocalDate tempDebut=chambreHotel[i].getReservation()[j].getDateDebut();
						LocalDate tempFin=chambreHotel[i].getReservation()[j].getDateFin();

						//les dates de la reservation sont passees en null pour ne pas interferer avec la recherche des dates disponibles
						chambreHotel[i].getReservation()[j].setDateDebut(null);
						chambreHotel[i].getReservation()[j].setDateFin(null);

						//creation d'une date temporelle pour savoir si entre les nouvelles dates la chambre est reservee
						LocalDate tempoNouvelleDate = nouvelleDateDebut;

						//tant que la date temporelle n'a pas atteint la nouvelle date de fin
						while (!tempoNouvelleDate.isAfter(nouvelleDateFin)) {

							//si la chambre est reservee
							if (chambreHotel[i].estReservee(tempoNouvelleDate)) {

								System.out.println("La Chambre est reservée durant cette periode.");

								//on remet les anciennes dates à leur emplacement.
								chambreHotel[i].getReservation()[j].setDateDebut(tempDebut);
								chambreHotel[i].getReservation()[j].setDateFin(tempFin);
								return false;
							}
							//tant que la date n'a pas rencontree de reservation elle augmente d'un jour a chaque boucle
							tempoNouvelleDate = tempoNouvelleDate.plusDays(1);
						}

						//si la date tempo a atteint la date de fin sans rencontrer d'autres reservation, on reserve.
						chambreHotel[i].getReservation()[j].setDateDebut(nouvelleDateDebut);
						chambreHotel[i].getReservation()[j].setDateFin(nouvelleDateFin);


						//1er cas la date actuelle est avant la reservation (la reservation n'as pas commencé)
						if(LocalDate.now().isBefore(tempDebut)) {
							nbdejoursrestants=nbDeJoursReservation2-nbDeJoursReservation1;
						}

						//2eme cas la date actuelle est pendant la premiere periode de reservation(la reservation a commencé)
						else if(!LocalDate.now().isBefore(tempDebut)&&!LocalDate.now().isAfter(tempFin)) {
							//si la nouvelle date est apres la fin de la 1ere reservation
							if(nouvelleDateDebut.isAfter(tempFin)) {
								System.out.println("Impossible de decaler la reservation en cours à une nouvelle date de debut.");
								boolean reponseValide=false;

								while(!reponseValide) {	
									System.out.println("Voulez vous (P)rolonger la reservation à la date suivante " + nouvelleDateFin+" ?" 
											+"\nPour creer une nouvelle (R)eservation veuillez passer par le service de reservation dans le menu principal");
									String choix=in.nextLine().toLowerCase();

									switch(choix.charAt(0)) {
									case 'p': 
										//si le cilent veut prolonger
										nbdejoursrestants=differenceDebut1etFin2-nbDeJoursReservation1;
										reponseValide=true;break;
										//sortir de la methode
									case 'r':return false;


									default: reponseValide=false;
									}

								}
							}
							//si la nouvelle date est dans la periode la 1ere reservation alors prolongation ou raccourci du sejour
							else {
								nbdejoursrestants=differenceDebut1etFin2-nbDeJoursReservation1;
							}
						}

						//calcul de nouveau tarif
						int nouveauTarif=chambreHotel[i].getTarif()*nbdejoursrestants;
						//generation transaction
						chambreHotel[i].getReservation()[j].generationTransactionBancaire(Verification.natureDeOperation(nbdejoursrestants), nouveauTarif, 'm', in);
						PDF.creationPdf(getChambreHotel()[i].getOneReservation(j).getClientReservation(), getChambreHotel()[i], this, getEmployeHotel()[0], LocalDate.now());
						Mail.creationMail(employeHotel[0], chambreHotel[i].getOneReservation(j).getClientReservation());
						return true;
					}
				}
				else {
					System.out.println("La Reservation est introuvable.");
					return false;	
				}	
			}
		}
		return false;
	}

	public boolean annulationReservation(int numeroChambre, LocalDate dateDebut, LocalDate dateFin, Scanner in) throws IOException {
		for (int i = 0; i < chambreHotel.length; i++) {

			for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {

				if (numeroChambre > 0 && chambreHotel[i].getOneReservation(j) != null) {

					if (numeroChambre == (chambreHotel[i].getNumeroChambre())
							&& dateDebut.equals(chambreHotel[i].getOneReservation(j).getDateDebut())
							&& dateFin.equals(chambreHotel[i].getOneReservation(j).getDateFin())) {

						
						
						int nbJoursARembourser = Period.between(dateFin, dateDebut).getDays();
		                int remboursement = chambreHotel[i].getTarif()*nbJoursARembourser;
		                
						chambreHotel[i].getReservation()[j].generationTransactionBancaire(Verification.natureDeOperation(nbJoursARembourser), remboursement, 'r', in);
						PDF.creationPdf(getChambreHotel()[i].getOneReservation(j).getClientReservation(), getChambreHotel()[i], this, getEmployeHotel()[0], LocalDate.now());
						Mail.creationMail(employeHotel[0], chambreHotel[i].getOneReservation(j).getClientReservation());
						chambreHotel[i].setOneReservation(null, j);
						
						System.out.println("Annulation effectuée avec succès");
						return true;
					}
				}
			}
		}
		System.out.println("La Reservation est introuvable");
		return false;
	}

	/**
	 * permet de lancer le programme
	 * 
	 * @param in le scanner
	 */
	public void launch(Scanner in) {
		while (true) {
			estUnClient(in);
		}
	}

	/**
	 * Affiche le menu et donne le choix a l'utilisateur
	 * 
	 * @param in le scanner
	 */
	public void choixMenu(Scanner in) {
		char reponse = ' ';
		while (reponse != 'Q') {
			affichageMenu();
			System.out.println("Choix :");
			reponse = in.nextLine().toUpperCase().charAt(0);
			switch (reponse) {
			case 'A': // Afficher l'etat de l'hotel
			{
				afficherEtatHotel();
				break;
			}
			case 'B': // Afficher le nombre de chambre réservées
			{
				System.out.println("a quelle date ?");
				boolean dateValide = false;
				String dateS = null;
				LocalDate date = null;
				while (!dateValide) {
					dateS = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateS))
						dateValide = false;
					else 
					{
						String []stockageDate  = dateS.split("/");
						date = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide = true;
					}
				}
				afficherNombreChambresReservees(date);
				break;
			}
			case 'C': // Afficher le nombre de chambre libre
			{
				System.out.println("a quelle date ?");
				boolean dateValide = false;
				String dateS = null;
				LocalDate date = null;
				while (!dateValide) {
					dateS = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateS))
						dateValide = false;
					else 
					{
						String []stockageDate  = dateS.split("/");
						date = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide = true;
					}
				}
				afficherNombreChambresLibres(date);
				break;
			}
			case 'D': // Afficher le numero de la premiere chambre vide
			{
				System.out.println("a quelle date ?");
				boolean dateValide = false;
				String dateS = null;
				LocalDate date = null;
				while (!dateValide) {
					dateS = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateS))
						dateValide = false;
					else 
					{
						String []stockageDate  = dateS.split("/");
						date = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide = true;
					}
				}
				System.out.println("premiere chambre libre :" + premiereChambreLibre(date));
				break;
			}
			case 'E': // Afficher le numero de la derniere chambre vide
			{
				System.out.println("a quelle date ?");
				boolean dateValide = false;
				String dateS = null;
				LocalDate date = null;
				while (!dateValide) {
					dateS = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateS))
						dateValide = false;
					else 
					{
						String []stockageDate  = dateS.split("/");
						date = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide = true;
					}
				}
				System.out.println("derniere chambre libre :" + derniereChambreLibre(date));
				break;
			}
			case 'F': // Réserver une chambre
			{
				try {
					if (!Verification.logAdminCorrect(in))
						break;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("a quelles dates ?");
				boolean dateValide1 = false;
				boolean dateValide2 = false;
				String dateD = null;
				String dateF = null;
				LocalDate dateDebut = null;
				LocalDate dateFin = null;
				while (!dateValide1 && !dateValide2) {
					System.out.println("Saisir les dates :");
					dateD = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateD))
						dateValide1 = false;
					else 
					{
						String []stockageDate  = dateD.split("/");
						dateDebut = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide1 = true;
					}
					dateF = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateF))
						dateValide2 = false;
					else 
					{
						String []stockageDate  = dateF.split("/");
						dateFin = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide2 = true;
					}
				}
				try {
					if (!reservationPremiereChambreLibre(dateDebut, dateFin, null, in))
						System.out.println("KO");
				} catch (IOException e) {
				}
				break;
			}
			case 'G': // Libérer une chambre
			{
				try {
					if (!Verification.logAdminCorrect(in))
						break;
					System.out.println("Liberation par 1-Nom ou par 2-Numero ?");
					int choix = in.nextInt();
					in.nextLine();
					switch (choix) {
					case 1:
						System.out.println("Saisir le nom du client :");
						String nomClient = in.nextLine();
						boolean dateValide1 = false;
						boolean dateValide2 = false;
						String dateD = null;
						String dateF = null;
						LocalDate dateDebut = null;
						LocalDate dateFin = null;
						while (!dateValide1 && !dateValide2) {
							System.out.println("Saisir les dates :");
							dateD = in.nextLine();
							if (!Verification.verifierSiUneSeuleDateValide(dateD))
								dateValide1 = false;
							else 
							{
								String []stockageDate  = dateD.split("/");
								dateDebut = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
								dateValide1 = true;
							}
							dateF = in.nextLine();
							if (!Verification.verifierSiUneSeuleDateValide(dateF))
								dateValide2 = false;
							else 
							{
								String []stockageDate  = dateF.split("/");
								dateFin = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
								dateValide2 = true;
							}
						}
						liberationChambre(nomClient, -1, dateDebut, dateFin);
						break;

					default:
						break;
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			}
			case 'H': // Modifier une réservation
			{
				try {
					if (!Verification.logAdminCorrect(in))
						break;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("Entrez l'identifiant du client:");
				String idclient = in.nextLine();
				
					System.out.println("Entrez la date du debut de la reservation à modifier (jj/mm/aaaa):");
					String dateDebut = in.nextLine();
					System.out.println("Entrez la date de fin de la reservation à modifier (jj/mm/aaaa):");
					String dateFin = in.nextLine();
					// si les deux dates entrés sont valides
					if (Verification.siDeuxDatesValidesEtDansBonSens(dateDebut, dateFin)) {

						// on continue pour connaitre les nouvelles dates souhaités
						System.out.println("Entrez la nouvelle date de debut:");
						String nouvelleDateDebutChoisie = in.nextLine();
						System.out.println("Entrez la nouvelle date de fin:");
						String nouvelleDateFinChoisie = in.nextLine();

						// si les nouvelles dates sont correctes et dans le bon sens

						if (Verification.siDeuxDatesValidesEtDansBonSens(nouvelleDateDebutChoisie, nouvelleDateFinChoisie))
						{
							try {

								// passage à la modification

								modificationReservation(idclient, Verification.transformerDate(dateDebut),
										Verification.transformerDate(dateFin),
										Verification.transformerDate(nouvelleDateDebutChoisie),
										Verification.transformerDate(nouvelleDateFinChoisie), in);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} 
						else {

							break;
						}
						break;
					}
					break;
				}
			
			
			case 'I': // Annuler une réservation
			{
				try {
					if (!Verification.logAdminCorrect(in))
						break;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("numero de chambre ?");
				int num = in.nextInt();
				in.nextLine();
				boolean dateValide1 = false;
				boolean dateValide2 = false;
				String dateD = null;
				String dateF = null;
				LocalDate dateDebut = null;
				LocalDate dateFin = null;
				while (!dateValide1 && !dateValide2) {
					System.out.println("Saisir les dates :");
					dateD = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateD))
						dateValide1 = false;
					else 
					{
						String []stockageDate  = dateD.split("/");
						dateDebut = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide1 = true;
					}
					dateF = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateF))
						dateValide2 = false;
					else 
					{
						String []stockageDate  = dateF.split("/");
						dateFin = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide2 = true;
					}
				}
				try {
					annulationReservation(num, dateDebut, dateFin, in);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			case 'J': // Afficher le chiffre d'affaire
			{
				System.out.println("a quelle date ?");
				boolean dateValide = false;
				String dateS = null;
				LocalDate date = null;
				while (!dateValide) {
					dateS = in.nextLine();
					if (!Verification.verifierSiUneSeuleDateValide(dateS))
						dateValide = false;
					else 
					{
						String []stockageDate  = dateS.split("/");
						date = LocalDate.parse(stockageDate[2]+"-"+stockageDate[1]+"-"+stockageDate[0]);
						dateValide = true;
					}
				}
				System.out.println("chiffre d'affaire :" + calculChiffreAffaire(date));
			}
			default:
				break;
			}
		}
	}

	/**
	 * Calcul du chiffre d'affaire a une date donnée
	 * 
	 * @param date
	 *            la date donnée
	 * @return le chiffre d'affaire en double
	 */
	public double calculChiffreAffaire(LocalDate date) {
		double chiffreAffaire = 0;
		for (int i = 0; i < chambreHotel.length; i++) {
			for (int j = 0; j < chambreHotel[i].getReservation().length; j++) {
				if (chambreHotel[i].estReservee(date) && chambreHotel[i].getOneReservation(j) != null) {
					long nbJours = ChronoUnit.DAYS.between(chambreHotel[i].getOneReservation(j).getDateDebut(),
							chambreHotel[i].getOneReservation(j).getDateFin());
					chiffreAffaire += chambreHotel[i].getTarif() * nbJours;
				}
			}
		}
		return chiffreAffaire;
	}
}
