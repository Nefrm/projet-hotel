# Projet Hotel Version 2  
* Groupe :  
	* Dorne Julien  
	* Zenina Alexandra  
	* Ouafrra Ali  

## Information  
Projet de l'hotel.  

Créer un répertoire ressources dans le meme répertoire que le .jar et y mettre le fichier d'initialisation et le fichier de mot de passe admin.  
Le fichier d'initialisation doit se nommer : "ListeChambres.csv".  
Le fichier de mot de passe admin doit se nommer : "LogAdmin.txt" et contenir le mot de passe sur la premiere ligne du fichier.  
Les Factures et les Transactions sont générées dans le répertoire ressources dans leur répertoire respectif.  

Le fichier d'initialisation doit etre de la forme à partir de la seconde ligne :  

* Type chambre;Taille;Vues;Occupation;tarif;Nombre;Options  

Les transactions sont de la forme :  

- Date de la transaction  
- Nature du paiment :  
	- P : paiement  
	- R : remboursement  
- Type de transaction :  
	- R : réservation  
	- A : annulation  
	- M : modification  
	- L : libération  
- Montant de la transaction  
- Numero de carte bancaire  

Projet non terminé.

## Lancement du Programme
Ouvrir l'invite de commande dans le répertoire du .jar.
>java -jar Hotel.jar

## Tâche  
- [x] Jar Runnable  
- [x] Cahier de Test  
- [x] Liste des Fonctions  

## Fonctionnalitées  
- [x] Créer d'un hôtel  
- [x] Réserver une chambre dans l'hôtel (protégée par un mot de passe)  
- [x] Annuler une réservation (protégée par un mot de passe)  
- [x] Libérer une chambre (protégée par un mot de passe)  
- [x] Modifier une réservation (protégée par un mot de passe)  
- [x] Créer un client  
- [x] Voir l'état de toute les chambres de l'hôtel  
- [x] Affichage d'un menu pour la navigation  
- [x] Login pour le client  
- [x] Login et mot de passe pour l'employé  
- [x] Envoi de Mail  
- [x] Création de facture  
- [x] Vérification
